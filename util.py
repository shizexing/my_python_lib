#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
# @author: Shi, Zexing
# Copyright (c) 2019. All rights reserved.

def execute(cmd):
    import subprocess
    p = subprocess.Popen('%s' % cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (output, err) = p.communicate()
    if 0 != p.returncode:
        print('cmd:', cmd)
        print('ret:', p.returncode)
        print('err:', err)
        exit(p.returncode)
    output = output.strip()
    return (p.returncode, output)

if __name__ == "__main__":
    pass