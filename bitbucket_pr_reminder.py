#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
# @author: Shi, Zexing
# Copyright (c) 2019. All rights reserved.

import argparse, os, re, sys, time

# reference : 
# list pull-requests : https://docs.atlassian.com/DAC/rest/stash/3.0.1/stash-rest.html#idp961536
# https://docs.atlassian.com/DAC/rest/stash/3.0.1/stash-rest.html#idp999792

def is_exipred_pr(ctime):
    '''
    @param ctime: create time
    return True if the pull request is created more than 7days
    '''
    curtime = time.time()
    if (curtime - ctime) < 7 * 24 * 60 * 60:
        return False
    return True

def inactive_pr(ctime):
    '''
    @param ctime: create time
    return True if the pull request is created more than 15days
    '''
    curtime = time.time()
    if (curtime - ctime) < 15 * 24 * 60 * 60:
        return False
    return True

def list_pr(user, passwd, author = None, projectkey = 'NAV', repositorySlug = 'navcore'):
    # https://docs.atlassian.com/DAC/rest/stash/3.0.1/stash-rest.html#idp961536
    # curl -L -u zexings@telenav.com:@Sunnyvale1 https://bitbucket.telenav.com/rest/api/latest/projects/NAV/repos/navcore/pull-requests\?limit\=100 -o pr.json
    cmd = 'curl -L -u %s:%s https://bitbucket.telenav.com/rest/api/latest/projects/%s/repos/%s/pull-requests\?limit\=100 -o pr.json' % (user, passwd, projectkey, repositorySlug)
    (rc, output) = execute(cmd)
    ids = []
    inactive_ids = []
    if 0 == rc:
        with open('pr.json') as f:
            # size
            # isLastPage
            # start
            # limit
            # values
            data = f.read()
            d = loads(data)
            print('open pull requests:%d' % d['size'])
            print('lastpage: %s' % d['isLastPage'])
            print('start: %d' % d['start'])
            print('limit: %d' % d['limit'])
            assert(d['size'] == len(d['values']))

            for pr in d['values']:
                if (author is not None) and (pr['author']['user']['name'] != author):
                    continue
                ctime = pr['createdDate'] / 1000.0
                if is_exipred_pr(ctime):
                    ids.append(pr['id'])
                if inactive_pr(ctime):
                    inactive_ids.append(pr['id'])
    else:
        return []
    return (sorted(ids), sorted(inactive_ids))

def notifyToMerge(user, prid):
    pass

def notifyConflict(user, prid, projectkey, repositorySlug):
    print('-' * 30, ' conflict on PR-%d ', '-' * 30)
    pr_url = 'https://bitbucket.telenav.com/projects/%s/repos/%s/pull-requests/%d/overview' % (projectkey, repositorySlug, prid)
    msg = 'conflict need to resolve for %s' % pr_url
    cmd = 'echo "%s" | mail -s "conflict on PR-%d" %s' % (msg, prid, user)
    print('cmd: \n%s' % cmd)
    # if sendEmail:
    #     print ('sending email....')
    #     os.system(cmd)
    os.system(cmd) 
    
def parse_reviewers(msg, ignore_reviewer = None):
    '''
    You need at least one approval from LuQin, Yinyin, Yafen or ZhangJie for tasdk DirectionService change
    You need at least one approval from Bingyang, Zelin or Yingxiao for mapdataaccess change
    You need at least one approval from Bingyang or Zexing for datamanager change
    You need at least one approval from Zexing for datamanager change
    You need at least one approval from Dmitry, Zexing, Mihai A Costea or BoSong for buildtools change
    '''
    reviewers = []
    mo = re.search(r'^You need at least one approval from (.*) for (?:(?:\w+ )*\w+) change$', msg)

    def append_reviewer(reviewers, reviewer, ignore_reviewer = ignore_reviewer):
        if reviewer == ignore_reviewer:
            return
        reviewers.append(reviewer)
    if mo:
        msg_reviewers = mo[1]
        or_divider = ' or '
        if or_divider in msg_reviewers:
            h1 = msg_reviewers[:msg_reviewers.find(or_divider)]
            last_reviewer = msg_reviewers[msg_reviewers.find(or_divider) + len(or_divider):]
            cv = h1.split(',')
            for v in cv:
                rvname = v.strip()
                append_reviewer(reviewers, rvname)
            append_reviewer(reviewers, last_reviewer)
        else:
            if msg_reviewers == ignore_reviewer:
                print('-' * 30, '%s is the only reviewer to review PR for him/her self!!!', '-' * 30)
            append_reviewer(reviewers, msg_reviewers)
    return reviewers

def remind_reviewers(prid, user, passwd, ignore_reviewer, projectkey = 'NAV', repositorySlug = 'navcore'):
    # https://bitbucket.telenav.com/rest/api/latest/projects/NAV/repos/navcore/pull-requests/2590/merge
    fn = 'merge_%d.json' % prid
    cmd = 'curl -L -u %s:%s https://bitbucket.telenav.com/rest/api/latest/projects/%s/repos/%s/pull-requests/%d/merge -o %s' % (user, passwd, projectkey, repositorySlug, prid, fn)
    # print('cmd : %s' % cmd)
    rc = 0
    (rc, output) = execute(cmd)
    if 0 == rc:
        with open(fn) as f:
            data = f.read()
            d = loads(data)
            # print('info:%s' % d)
            if d['canMerge']:
                notifyToMerge(user, prid)
                return None
            # canMerge
            # conflicted
            if d['conflicted']:
                notifyConflict(user, prid, projectkey, repositorySlug)
                return None
            # outcome
            # vetoes
            component2reviewers = {}
            for veto in d['vetoes']:
                summery = veto['summaryMessage']
                mo = re.search(r'Approval required for ((?:\w+ )*\w+) change', summery)
                project = None
                if mo:
                    project = mo[1]
                    msg = veto['detailedMessage']
                    reviewers = parse_reviewers(msg, ignore_reviewer)
                    # You need at least one approval from Bingyang, Zelin or Yingxiao for mapdataaccess change
                    # You need at least one approval from Bingyang or Zexing for datamanager change
                    # You need at least one approval from LuQin, Yinyin, Yafen or ZhangJie for tasdk DirectionService change
                    component2reviewers[project] = reviewers
            return component2reviewers

    else:
        print('failed to get merge information for PR-%d' % prid)
    print()

def email_reminder(pr_reminders, inactive_ids, username, sendEmail, projectkey = 'NAV', repositorySlug = 'navcore'):
    from name2email import name2address
    # for pr in pr_reminders.keys():
    for prid in pr_reminders:
        pr_url = 'https://bitbucket.telenav.com/projects/%s/repos/%s/pull-requests/%d/overview' % (projectkey, repositorySlug, prid)
        reviewer_names = []
        reviewer_addresses = []
        component_reviewer_addrs = {}
        cp2msg = []
        for cp in pr_reminders[prid]:
            cp_reviewer_addr = []
            for name in pr_reminders[prid][cp]:
                addr = name2address[name]
                cp_reviewer_addr.append(addr)
                if addr not in reviewer_addresses:
                    reviewer_addresses.append(addr)
                if name not in reviewer_names:
                    reviewer_names.append(name)
            component_reviewer_addrs[cp] = cp_reviewer_addr
            
            cp2msg.append(' ' * 4 + '%s:' % cp)
            cp2msg.append(' ' * 8 + ', '.join(pr_reminders[prid][cp]))
            # cp2msg.append(' ' * 4 + ', '.join(pr_reminders[prid][cp]) + ' for %s' % cp)
        msg = ''
        # msg += '<html><body>\n'
        msg += 'Hi, %s\n\n' % ', '.join(reviewer_names)
        msg += ' ' * 4 + 'Please help to review %s\n\n' % pr_url
        msg += '\n'.join(cp2msg)
        msg += '\n\nThanks\n%s' % username
        # msg += '</body></html>\n'
        # print('msg: \n%s' % msg)
        # print('addresses: \n%s' % ','.join(reviewer_addresses))
        bcc = 'zexings@telenav.com'
        if prid in inactive_ids:
            # bcc += ',bsong@telenav.com,serenaw@telenav.com,shzhjiang@telenav.cn,xizhens@telenav.com'
            pass
        cmd = 'echo "%s" | mail -s "Help review PR-%d" -b %s %s' % (msg, prid, bcc, ','.join(reviewer_addresses))
        print('cmd: \n%s' % cmd)
        if sendEmail:
            print ('%ssending email.... %s' % (bcolors.OKBLUE, bcolors.ENDC))
            os.system(cmd)
        else:
            print('%s%s skip sending email %s%s' % (bcolors.WARNING, '*' * 30, '*' * 30, bcolors.ENDC))

if __name__ == "__main__":
    oldpwd = os.getcwd()
    sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)))
    os.chdir(os.path.dirname(os.path.realpath(__file__)))

    def test_parse_reviewers(msg):
        print('msg: %s' % msg)
        l = parse_reviewers(msg)
        print(l)
        print()
    # msg = 'You need at least one approval from LuQin, Yinyin, Yafen or ZhangJie for tasdk DirectionService change'
    # test_parse_reviewers(msg)
    # msg = 'You need at least one approval from Bingyang, Zelin or Yingxiao for mapdataaccess change'
    # test_parse_reviewers(msg)
    # msg = 'You need at least one approval from Bingyang or Zexing for datamanager change'
    # test_parse_reviewers(msg)
    # msg = 'You need at least one approval from Zexing for datamanager change'
    # test_parse_reviewers(msg)
    # msg = 'You need at least one approval from Dmitry, Zexing, Mihai A Costea or BoSong for buildtools change'
    # test_parse_reviewers(msg)
    # exit(0)

    from util import execute
    from jsonutils import loads
    from color import bcolors

    user = None
    username = None
    passwd = None
    ignore_reviewer = None      # ignore self 

    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--userid', help = 'user account to login git repo')
    parser.add_argument('-p', '--password')
    parser.add_argument('-n', '--username')
    parser.add_argument('-i', '--ignore_reviewer', help = 'ignore self')
    parser.add_argument('-m', '--email', default = False, help = 'sending email', action='store_true')
    args = parser.parse_args()
    
    if None != args.userid:
        user = args.userid
    if None != args.password:
        passwd = args.password
    if args.username:
        username = args.username
    
    sending_email = False
    if args.email:
        sending_email = True
    
    # user = 'zexings@telenav.com'
    # username = 'Zexing'
    # ignore_reviewer = 'Zexing'

    if user is None:
        print('invalid userid for access git repo')
        parser.print_help()
        exit(1)
    if passwd is None:
        print('invalid password for access git repo')
        parser.print_help()
        exit(1)
    if username is None:
        username = user
    if ignore_reviewer is None:
        ignore_reviewer = username
    
    print('userid:%s' % user)
    print('password:%s' % passwd)
    print('username:%s' % username)
    print('ignore:%s' % ignore_reviewer)
    
    # exit(0)

    (ids, inactive_ids) = list_pr(user, passwd, user)
    print('pull-requests : %s' % ids)
    # ids = [2590]
    # ids.remove(2587)
    # ids.remove(2590)
    d = {}
    for id in ids:
        c2r = remind_reviewers(id, user, passwd, ignore_reviewer)
        if c2r:
            print ('PR-%d : %s' % (id, c2r))
            d[id] = c2r
    # print('pr + reviewers : %s' % d)

    email_reminder(d, inactive_ids, username, args.email)

    os.chdir(oldpwd)
