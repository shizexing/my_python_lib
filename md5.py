#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
# @author: Shi, Zexing
# Copyright (c) 2018. All rights reserved.

import hashlib

def md5(s):
    # ref: https://stackoverflow.com/questions/5297448/how-to-get-md5-sum-of-a-string-using-python
    return hashlib.md5(s).hexdigest()

def md5f(fn):
    # https://stackoverflow.com/questions/48122798/oserror-errno-22-invalid-argument-when-reading-a-huge-file
    m = hashlib.md5()
    with open(fn, 'rb') as f:
        while True:
            block = f.read(64 * (1 << 20))  # read 64MB
            if not block:   # EOF
                break
            m.update(block)
    return m.hexdigest()
