#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
# @author: Shi, Zexing
# Copyright (c) 2017. All rights reserved.

import json

# python2
# https://docs.python.org/dev/howto/pyporting.html
# http://python3porting.com/
# https://wiki.python.org/moin/Python3.0#Built-In_Changes
# https://stackoverflow.com/questions/19877306/nameerror-global-name-unicode-is-not-defined-in-python-3
# https://stackoverflow.com/questions/30418481/error-dict-object-has-no-attribute-iteritems

# reference : http://stackoverflow.com/questions/956867/how-to-get-string-objects-instead-of-unicode-ones-from-json-in-python
def _byteify(data, ignore_dicts = False):
    # in python3, unicode is replaced by str
    if isinstance(data, str):
        return data
    # if isinstance(data, unicode):
        # if this is unicode string, return its utf-8 representation
        # NOTE: zexing, in python2, encode to utf-8
        # return data.encode('utf-8')
    # if this is a list of values, return list of byteified values
    if isinstance(data, list):
        return [ _byteify(item, ignore_dicts=True) for item in data ]
    # if this is a dictionary, return dictionary of byteified keys and values
    # but only if we haven't already byteified it
    if isinstance(data, dict) and not ignore_dicts:
        return {
            # _byteify(key, ignore_dicts=True): _byteify(value, ignore_dicts=True) for key, value in data.iteritems()
            _byteify(key, ignore_dicts=True): _byteify(value, ignore_dicts=True) for key, value in data.items()
        }
    # if it's anything else, return it in its original form
    return data

class jsonutils(object):
    def loads(self, str):
        ''' loads json string and convert unicode -> utf-8
        @return dictionary or list
        '''
        return _byteify(
                json.loads(str, object_hook=_byteify),
                ignore_dicts = True
                )
    
    def dumps(self, data, indent=None):
        ''' get json representation for the data
        @param data: data to serielize
        @return: json representation
        '''
        return json.dumps(data, ensure_ascii=False, indent=indent)

def loads(str):
    ''' loads json string and convert unicode -> utf-8
    @return dictionary or list
    '''
    return jsonutils().loads(str)

def dumps(data, indent=None):
    ''' get json representation for the data
    @param data: data to serielize
    @return: json representation
    '''
    return jsonutils().dumps(data, indent)

if __name__ == '__main__':
    od = {'lat':12.345,'lon':-24.22,'wayid':'abc'}
    js = json.dumps(od, ensure_ascii=False)
    print(js)
    d = loads(js)
    print(d)
    print(json.loads(js))
    # f = open('cases/3_tollcase_771923017100.json')
    # s = f.read()
    # print('s:%s' % s)
    # print(loads(s))
    
    d = loads('')
    print('empty string : %s' % d)