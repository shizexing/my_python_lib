#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
# @author: Shi, Zexing
# Copyright (c) 2019. All rights reserved.

# NOTE: 
#   *1. parse pr.json to get address --- This is not working, e.g: Qu, qi. 
#       Qu, qi's name used in reviewer is 'Quqi'
#       but his profile is 'Qu, Qi', 'qqu@telenav.cn'
#   2. create a mapping json file
#   3. query user with REST API --- cannot query with Quqi
#       e.g.: https://bitbucket.telenav.com/rest/api/latest/users?filter=Quqi  --- not working
#           https://bitbucket.telenav.com/rest/api/latest/users/zexings_telenav.com --- OK
#           https://bitbucket.telenav.com/rest/api/latest/users?filter=zexing   --- OK
name2address = {
    'Bingyang': 'bywang@telenav.cn',
    'Bo': 'bsong@telenav.com',
    'BoSong': 'bsong@telenav.com',
    'Dmitry': 'dmitrya@telenav.com',
    'LiXiang': 'xli2@telenav.cn',       # Bad guy
    'LuQin': 'greenlu@telenav.cn',
    'Mihai A Costea': 'mihai.costea@telenav.com',
    'Mingzhou': 'mzhli@telenav.cn',
    'Quqi': 'qqu@telenav.cn',           # Bad guy, cannot find name->email mapping
    'QuXing': 'xqu@telenav.cn',         # Bad guy
    'Xufeng': 'xfzhang@telenav.cn',
    'XunLiu': 'xunliu@telenav.com',
    'Yafen': 'yfding@telenav.cn',
    'Yibin': 'yblin@telenav.cn',
    'Yingxiao': 'yxzhang@telenav.cn',
    'Yinyin': 'yyzhou@telenav.cn',
    'Zelin': 'zlliu@telenav.cn',
    'Zexing': 'zexings@telenav.com',
    'ZhangJie': 'jiezhang2@telenav.cn', # Bad guy
}
