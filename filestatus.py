#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
# @author: Shi, Zexing
# Copyright (c) 2018. All rights reserved.

import os

# C:\Users\zexings\Downloads\software\python-3.6.1-embed-amd64\python.exe filestatus.py
'''
def codec_detect(s):
    codecs = ['ascii', 'gb18030', 'big5', 'big5hkscs', 'cp932', 'cp950', 'cp65001', 'euc_jp', 'euc_jis_2004', 'euc_jisx0213', 'gb2312', 'gbk', 'hz']
    for c in codecs:
        try:
            ds = s.decode(c)
            print ('%s is codec %s' % (s, c))
            return
        except:
            pass
'''

'''
I encounter similar issue for python 2.7

the root cause for my case is a special character

・
https://unicode-table.com/en/30FB/

Then I tried with python 3.6
python can detect the character correctly.
'''

KEY_FILENAME    = 'filename'
KEY_FULLPATH    = 'fullpath'
KEY_MD5         = 'md5'
KEY_MTIME       = 'mtime'
KEY_RELPATH     = 'relpath'
KEY_SIZE        = 'size'

class file_filter:
    ''' filter files
    '''
    def __init__(self):
        self.filenames  = ['.DS_Store']
        self.suffixes   = ['.url', '.URL', '.mht', '.HTML']
        self.prefixes   = ['_____padding_file_']
    
    def filter(self, filename):
        if filename in self.filenames:
            return True
        for suffix in self.suffixes:
            if filename.endswith(suffix):
                return True
        for prefix in self.prefixes:
            if filename.startswith(prefix):
                return True
        return False

def pathname_google_drive():
    KEY_GOOGLE_DRIVE = 'GOOGLE_DRIVE_HOME'
    #if not os.environ.has_key(KEY_GOOGLE_DRIVE):
    if KEY_GOOGLE_DRIVE not in os.environ:
        raise Exception('environment %s not set' % KEY_GOOGLE_DRIVE)
    gd_home = os.environ[KEY_GOOGLE_DRIVE]
    print ('%s=%s' % (KEY_GOOGLE_DRIVE, gd_home))
    if not os.path.isdir(gd_home):
        raise Exception('google drive home - %s is not a directory' % gd_home)
    return gd_home

def filename_filestatus_reserved():
    ''' filename which store filestatus in reserved directory
    '''
    fs_home = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'filestatus')
    return os.path.join(fs_home, 'filestatus_reserved.json')

def filename_filestatus_other():
    ''' filename which store filestatus not in reserved directory.
    e.g.: deleted, new downloaded
    '''
    fs_home = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'filestatus')
    return os.path.join(fs_home, 'filestatus_other.json')

def list_files(rootdir):
    ret = []
    rootdir = os.path.abspath(rootdir)

    filefilter = file_filter()
    # print('root directory : %s' % rootdir)
    for subdir, dirs, files in os.walk(rootdir):
        # print(files)
        for fn in files:
            if filefilter.filter(fn):
                continue
            fullpath = os.path.join(subdir, fn).replace('\\', '/')
            relpath = os.path.relpath(fullpath, rootdir).replace('\\', '/')

            ret.append({
                KEY_FILENAME: fn,
                KEY_FULLPATH: fullpath,
                KEY_RELPATH : relpath
            })
    return ret

def filestatus_by_file_info(filename, fullpath, relpath):
    print('get file status for : %s' % relpath)
    fs = os.stat(fullpath)
    m = ''
    # with open(fullpath, 'rb') as f:
    #     from md5 import md5
    #     s = f.read()
    #     m = md5(s)
    from md5 import md5f
    m = md5f(fullpath)
    return {
        KEY_FILENAME: filename,
        KEY_FULLPATH: fullpath,
        KEY_MD5     : m,
        KEY_MTIME   : fs.st_mtime,
        KEY_RELPATH : relpath,
        KEY_SIZE    : fs.st_size
    }

def filestatus_by_fileinfo(fileinfo):
    return filestatus_by_file_info(fileinfo[KEY_FILENAME], fileinfo[KEY_FULLPATH], fileinfo[KEY_RELPATH])

def filestatus_by_path(rootdir):
    ret = []
    d = os.path.abspath(rootdir)
    filefilter = file_filter()
    # print('root directory : %s' % d)
    for subdir, dirs, files in os.walk(rootdir):
        # print(files)
        for fn in files:
            if filefilter.filter(fn):
                continue
            fullpath = os.path.join(subdir, fn).replace('\\', '/')
            relpath = os.path.relpath(fullpath, rootdir).replace('\\', '/')            
            ret.append(filestatus_by_file_info(fn, fullpath, relpath))
    return ret

def filestatus_by_list(fileinfos):
    ret = []
    for fileinfo in fileinfos:
        ret.append(filestatus_by_fileinfo(fileinfo))
    return ret

def load_file_status(status_fn):
    ''' load filestatus.json - status_fn
    '''
    with open(status_fn, 'rb') as f:
        import jsonutils
        a = jsonutils.loads(f.read())
        return a
    return []

def dump_filestatus(fs, fn):
    bn = os.path.basename(fn)
    d = os.path.dirname(fn)
    (filename, ext) = os.path.splitext(bn)
    print ('fn: %s' % fn)
    # print('filename:%s' % bn)
    # print('dir:%s' % d)
    # print('filename:%s' % filename)
    # print('extension:%s' % ext)

    wfn = fn
    # wfn = os.path.join(d, '%s-%s%s' % (filename, sys.platform, ext))
    #if 'darwin' == sys.platform:
    #    wfn = os.path.join(d, '%s-osx%s' % (filename, ext))
    #if 'win32' == sys.platform:
    #	wfn = os.path.join(d, '%s
    print('output filename:%s' % wfn)
    #return
    with open(wfn, 'wb') as f:
        import jsonutils
        s = jsonutils.dumps(fs, 4)
        # f.write(s)  # 'w'
        f.write(s.encode('utf-8'))    # write() argument must be str, not bytes if 'w'

def check_duplicate(filename, fullpath, relpath):
    fs = os.stat(fullpath)
    oldfs = None
    if fullpath in fs_by_fullpath:
        oldfs = fs_by_fullpath[fullpath]
        if fs.st_mtime == oldfs[KEY_MTIME] and fs.st_size == oldfs[KEY_SIZE]:
            return [True, None]
    elif filename in fs_by_filename:
        oldfses = fs_by_filename[filename]
        for oldfs in oldfses:
            # in osx, the mtime is different than windows
            if int(fs.st_mtime) == int(oldfs[KEY_MTIME]) and fs.st_size == oldfs[KEY_SIZE]:
                return [True, None]
    filest = filestatus_by_file_info(filename, fullpath, relpath)
    if filest[KEY_MD5] in fs_by_md5:
        oldfses = fs_by_md5[filest[KEY_MD5]]
        for oldfs in oldfses:
            print('compare %s\n' % filest)
            print('with    %s\n' % oldfs)
            if filest == oldfs:
                print('same md5 !!!')
                return [True, filest]
            elif filest[KEY_FILENAME] == oldfs[KEY_FILENAME] and filest[KEY_MD5] == oldfs[KEY_MD5] and filest[KEY_SIZE] == oldfs[KEY_SIZE]:
                print('same md5 !!!')
                return [True, filest]
            elif filest[KEY_MD5] == oldfs[KEY_MD5] and filest[KEY_SIZE] == oldfs[KEY_SIZE]:
                print('????\n')
                exit(0)
    return [False, filest]

def check_update(path, show_duplicate = False):
    ''' check if the path has new files
    '''
    fileinfos = list_files(path)
    print('files = %d' % len(fileinfos))
    print('fs_by_fullpath.len = %d' % len(fs_by_fullpath))
    print('fs_by_filename.len = %d' % len(fs_by_filename))

    updates = []
    for fileinfo in fileinfos:
        filename = fileinfo[KEY_FILENAME]
        fullpath = fileinfo[KEY_FULLPATH]
        relpath  = fileinfo[KEY_RELPATH]
        [newfile, fs] = check_duplicate(filename, fullpath, relpath)
        if not newfile:
            updates.append(fs)
    
    return updates

if __name__ == '__main__':
    import sys
    from sys import exit
    import platform
    sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)))

    print ('sys.version_info:%s, major:%s' % (sys.version_info, sys.version_info.major))
    print('os.name:%s' % os.name)
    print('sys.platform:%s' % sys.platform)
    print('platform.system():%s' % platform.system())
    if 2 == sys.version_info.major:
        print('python3 is required to handle some characters and use utf-8 encoding by default')
        exit(1)
    print ('encoding:%s' % sys.getdefaultencoding())
    #reload(sys)
    #sys.setdefaultencoding('utf-8')
    #print 'encoding:%s' % sys.getdefaultencoding()

    KEY_RESERVE = 'ZX_RESERVES'
    #if not os.environ.has_key(KEY_RESERVE):
    if KEY_RESERVE not in os.environ:
        print ('environment %s not set' % KEY_RESERVE)
        exit(1)
    
    print ('%s=%s' % (KEY_RESERVE, os.environ[KEY_RESERVE]))
    reserves = os.environ[KEY_RESERVE].split(os.pathsep)
    print('reserves:%s' % reserves)
    # exit(0)
    # res_dir = os.environ[KEY_RESERVE]
    # res_dir = os.path.normpath(os.path.join(res_dir, 'Animation'))

    step = 0
    # 1. read previous file status
    step += 1
    print('\n%s %d. read reserved file status %s' %('-' * 20, step, '-' * 20))
    filestatus_fn = filename_filestatus_reserved()
    filestatuses_reserved = load_file_status(filestatus_fn)
    fs_by_fullpath = {}
    fs_by_filename = {}
    fs_by_md5 = {}
    for fs in filestatuses_reserved:
        fs_by_fullpath[fs[KEY_FULLPATH]]    = fs
        if fs[KEY_FILENAME] in fs_by_filename:
            fs_by_filename[fs[KEY_FILENAME]].append(fs)
            # print('same filename : %s\n' % fs_by_filename[fs[KEY_FILENAME]])
        else:
            fs_by_filename[fs[KEY_FILENAME]] = [fs]
        if fs[KEY_MD5] in fs_by_md5:
            fs_by_md5[fs[KEY_MD5]].append(fs)
        else:
            fs_by_md5[fs[KEY_MD5]] = [fs]
    print('filestatuses-reserved.len:%d' % len(filestatuses_reserved))

    # 2. check reserved directory update
    step += 1
    print('\n%s %d. check reserved folder update %s' %('-' * 20, step, '-' * 20))
    for res_dir in reserves:
        if len(res_dir) == 0:
            continue
        fs_res = check_update(res_dir)
        print('update on reserved directory: %s ? %d' % (res_dir, len(fs_res)))
        if len(fs_res) > 0:
            filestatuses_reserved = filestatuses_reserved + fs_res
            dump_filestatus(filestatuses_reserved, filename_filestatus_reserved())
            print('update reserved. rerun application for further update!!!')
            exit(0)
    
    # 3. load filestatus other
    step += 1
    print('\n%s %d. read other file status %s' %('-' * 20, step, '-' * 20))
    filestatuses_other = []
    if os.path.isfile(filename_filestatus_other()):
        filestatuses_other = load_file_status(filename_filestatus_other())
    print('filestatuses-other.len:%d' % len(filestatuses_other))
    for fs in filestatuses_other:
        fs_by_fullpath[fs[KEY_FULLPATH]]    = fs
        if fs[KEY_FILENAME] in fs_by_filename:
            fs_by_filename[fs[KEY_FILENAME]].append(fs)
            # print('same filename : %s\n' % fs_by_filename[fs[KEY_FILENAME]])
        else:
            fs_by_filename[fs[KEY_FILENAME]] = [fs]
        if fs[KEY_MD5] in fs_by_md5:
            fs_by_md5[fs[KEY_MD5]].append(fs)
        else:
            fs_by_md5[fs[KEY_MD5]] = [fs]
    print('len fullpath:%d' % len(fs_by_fullpath))
    print('len filename:%d' % len(fs_by_filename))
    print('len md5:%d' % len(fs_by_md5))

    # 4. check update for google drive mv
    step += 1
    print('%s %d. check mv folder update %s' %('-' * 20, step, '-' * 20))
    gdmv_dir = os.path.join(pathname_google_drive(), 'mv')
    fs_gdmv = check_update(gdmv_dir)
    print('update on google drive mv : %d' % len(fs_gdmv))
    if len(fs_gdmv) > 0:
        filestatuses_other = filestatuses_other + fs_gdmv
        dump_filestatus(filestatuses_other, filename_filestatus_other())
        print('update on google drive mv. rerun script for further update!!!')
        exit(0)

    # 5. scan new files
    step += 1
    print('\n%s %d. check no read new file %s' %('-' * 20, step, '-' * 20))
    gdnew_dir = os.path.join(pathname_google_drive(), 'new')
    fs_gdnew = check_update(gdnew_dir)
    print('update on gdnew_dir ? %d' % len(fs_gdnew))
    fi_gdnew = list_files(gdnew_dir)
    if len(fi_gdnew) != len(fs_gdnew):
        fullpaths = set(map(lambda fi : fi[KEY_FULLPATH], fi_gdnew))
        for fs in fs_gdnew:
            if fs[KEY_FULLPATH] in fullpaths:
                continue
            print('duplicate file: %s' % fs)
        exit(0)
    else:
        if len(fs_gdnew) > 0:
            print('all the new files are not read. good to move to mv')
            dump_filestatus(fs_gdnew, os.path.join(pathname_google_drive(), 'fs_new.json'))

    # 6. check update for google_drive_home reserved
    step += 1
    print('\n%s %d. remove gd reserved file from status %s' %('-' * 20, step, '-' * 20))
    gdres_dir = os.path.join(pathname_google_drive(), 'reserved')
    fileinfos_gdres = list_files(gdres_dir)
    fs_removed = []
    for fi in fileinfos_gdres:
        if fi[KEY_FILENAME] in fs_by_filename:
            fses = fs_by_filename[fi[KEY_FILENAME]]
            fs = None
            if len(fses) == 1:
                fs = fses[0]
            elif len(fses) > 0:
                cfs = filestatus_by_fileinfo(fi)
                for tfs in fses:
                    if cfs[KEY_FILENAME] == tfs[KEY_FILENAME] and cfs[KEY_MD5] == tfs[KEY_MD5] and cfs[KEY_SIZE] == tfs[KEY_SIZE]:
                        fs = tfs
                        break
            if fs:
                if fs in filestatuses_other:
                    filestatuses_other.remove(fs)
                    fs_removed.append(fs)
                else:
                    print('%s cannot found in others' % fi[KEY_FILENAME])
            else:
                print('no match in filestatues for %s' % fi[KEY_FILENAME])

    if len(fs_removed) > 0:
        if len(fs_removed) == len(fileinfos_gdres):
            print('remove %d from others' % len(fs_removed))
            for fs in fs_removed:
                print('remove %s' % fs[KEY_FILENAME])
            dump_filestatus(filestatuses_other, filename_filestatus_other())
            print('remove reserved from other. rerun script!!!')
            exit(0)
        else:
            for fs in fs_removed:
                fn = fs[KEY_FILENAME]
                for fi in fileinfos_gdres:
                    if fn == fi[KEY_FILENAME]:
                        fileinfos_gdres.remove(fi)
                        break
            
            print('not removed: %d' % len(fileinfos_gdres))
            for fi in fileinfos_gdres:
                print('%s' % fi)

    print('\n%s done %s' % ('-'*20, '-'*20))
    exit(0)
    
#  C:\Users\zexings\Downloads\software\python-3.6.1-embed-amd64\python.exe