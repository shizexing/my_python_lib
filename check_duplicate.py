#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
# @author: Shi, Zexing
# Copyright (c) 2018. All rights reserved.

import os

if __name__ == '__main__':
    import sys
    sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)))

    KEY_GOOGLE_DRIVE = 'GOOGLE_DRIVE_HOME'
    #if not os.environ.has_key(KEY_GOOGLE_DRIVE):
    if KEY_GOOGLE_DRIVE not in os.environ:
        print ('environtment %s not set' % KEY_GOOGLE_DRIVE)
        exit(2)
    gd_home = os.environ[KEY_GOOGLE_DRIVE]
    if not os.path.isdir(gd_home):
        print ('%s not a directory' % gd_home)
        exit(3)
    print ('%s=%s' % (KEY_GOOGLE_DRIVE, os.environ[KEY_GOOGLE_DRIVE]))

    fn = os.path.join(gd_home, 'filestatus.json')
    with open(fn, 'rb') as f:
        import jsonutils
        a = jsonutils.loads(f.read())
    print('a.size = %d' % len(a))
    items = {}
    for item in a:
        key = item['md5']
        if key in items:
            tmp = items[key]
            # if tmp['size'] == item['size']:
            print('--- duplicate ---')
            print('item1:%s' % tmp)
            print('item2:%s' % item)
        items[key] = item

    # add new files
    gd_res_dir = os.path.join(gd_home, 'reserved')
    from filestatus import filestatus
    fs2 = filestatus(gd_res_dir)
    # print('fs2:%s' % fs2)
    for s in fs2:
        print(s)
        if not os.path.isfile(s['fullpath']):
            print('%s not a file!!!' % s['fullpath'])
        key = s['md5']
        if key in items:
            tmp = items[key]
            print('--- duplicate ---')
            print('item1:%s' % tmp)
            print('itemN:%s' % s)
            continue
        items[key] = s
#  C:\Users\zexings\Downloads\software\python-3.6.1-embed-amd64\python.exe